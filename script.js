"use strict";
const btnFirstgrp = document.querySelector(".btn-first-group");
const btnSecondgrp = document.querySelector(".btn-second-group");

const btn0 = document.querySelector("#btn0");
const btn1 = document.querySelector("#btn1");
const btn2 = document.querySelector("#btn2");

const btnA = document.querySelector("#btnA");
const btnB = document.querySelector("#btnB");
const btnC = document.querySelector("#btnC");
const btnD = document.querySelector("#btnD");

// reset or init
const init = function () {
  showFirstMenu();
  hideSecondMenu();
};

const hideFirstMenu = function () {
  btn0.style.display = "none";
  btn1.style.display = "none";
  btn2.style.display = "none";
};

const showFirstMenu = function () {
  btn0.style.display = "block";
  btn1.style.display = "block";
  btn2.style.display = "block";
};

const hideSecondMenu = function () {
  btnA.style.display = "none";
  btnB.style.display = "none";
  btnC.style.display = "none";
  btnD.style.display = "none";
};

const showSecondMenu = function () {
  btnA.style.display = "block";
  btnB.style.display = "block";
  btnC.style.display = "block";
  btnD.style.display = "block";
};

init();

///// First menu

// withdraw button
btn0.addEventListener("click", () => {
  hideFirstMenu();
  showSecondMenu();
});

// deposit button
btn1.addEventListener("click", () => {
  // function call for deposit here
  console.log("deposit callback");
});

// cancel button
btn2.addEventListener("click", () => {
  console.log("cancel callback");
});

////// Second or sub menu

// back button
btnD.addEventListener("click", () => {
  hideSecondMenu();
  showFirstMenu();
});

// btn1.addEventListener("click", () => {
//   btn0.classList.add("hidden");
// });

// btn0.addEventListener("click", () => {
//   btn0.classList.add("hidden");
// });
